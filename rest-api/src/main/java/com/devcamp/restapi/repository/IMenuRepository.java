package com.devcamp.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.restapi.model.CMenu;

public interface IMenuRepository extends JpaRepository<CMenu, Long> {

}
