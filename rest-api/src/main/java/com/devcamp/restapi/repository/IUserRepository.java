package com.devcamp.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.restapi.model.CUser;

public interface IUserRepository extends JpaRepository<CUser, Long> {

}
