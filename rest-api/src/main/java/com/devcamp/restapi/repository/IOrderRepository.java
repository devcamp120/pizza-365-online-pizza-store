package com.devcamp.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.restapi.model.COrder;

public interface IOrderRepository extends JpaRepository<COrder, Long> {
    
}
