package com.devcamp.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.restapi.model.CDrink;

public interface IDrinkRepository extends JpaRepository<CDrink, Long> {
    CDrink findById(long id);
}
