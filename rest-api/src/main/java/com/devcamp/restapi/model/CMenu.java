package com.devcamp.restapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "menus")
public class CMenu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull(message = "Nhập tên menu!")
    @Column(name = "menu_name", unique = true)
    private String menuName;

    @NotEmpty(message = "Nhập đường kính (cm)!")
    @Column(name = "duong_kinh")
    private int duongKinhCM;

    @NotEmpty(message = "Nhập số lượng sườn nướng!")
    @Column(name = "suon_nuong")
    private int suonNuong;

    @NotEmpty(message = "Nhập khối lượng salad (gram)!")
    @Column(name = "salad")
    private int saladGr;

    @NotEmpty(message = "Nhập số lượng nước uống")
    @Column(name = "drink")
    private int drink;

    @NotEmpty(message = "Nhập giá menu (VND)!")
    @Column(name = "price")
    private int priceVND;

    public CMenu() {
    }

    public CMenu(String menuName, int duongKinhCM, int suonNuong, int saladGr, int drink, int priceVND) {
        this.menuName = menuName;
        this.duongKinhCM = duongKinhCM;
        this.suonNuong = suonNuong;
        this.saladGr = saladGr;
        this.drink = drink;
        this.priceVND = priceVND;
    }

    public CMenu(long id, String menuName, int duongKinhCM, int suonNuong, int saladGr, int drink, int priceVND) {
        this.id = id;
        this.menuName = menuName;
        this.duongKinhCM = duongKinhCM;
        this.suonNuong = suonNuong;
        this.saladGr = saladGr;
        this.drink = drink;
        this.priceVND = priceVND;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public int getDuongKinhCM() {
        return duongKinhCM;
    }

    public void setDuongKinhCM(int duongKinhCM) {
        this.duongKinhCM = duongKinhCM;
    }

    public int getSuonNuong() {
        return suonNuong;
    }

    public void setSuonNuong(int suonNuong) {
        this.suonNuong = suonNuong;
    }

    public int getSaladGr() {
        return saladGr;
    }

    public void setSaladGr(int saladGr) {
        this.saladGr = saladGr;
    }

    public int getDrink() {
        return drink;
    }

    public void setDrink(int drink) {
        this.drink = drink;
    }

    public int getPriceVND() {
        return priceVND;
    }

    public void setPriceVND(int priceVND) {
        this.priceVND = priceVND;
    }
}
