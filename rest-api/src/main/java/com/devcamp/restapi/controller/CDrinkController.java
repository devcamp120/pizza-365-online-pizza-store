package com.devcamp.restapi.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapi.model.CDrink;
import com.devcamp.restapi.repository.IDrinkRepository;

@CrossOrigin
@RestController
@RequestMapping
public class CDrinkController {
    @Autowired
    private IDrinkRepository pDrinkRepository;

    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllDrinks() {
        try {
            List<CDrink> pDrinks = new ArrayList<CDrink>();
            pDrinkRepository.findAll().forEach(pDrinks::add);
            return new ResponseEntity<>(pDrinks, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/drinks/{id}")
    public ResponseEntity<CDrink> getDrinkById(@PathVariable("id") long id) {
        try {
            CDrink drink = pDrinkRepository.findById(id);
            return new ResponseEntity<>(drink, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/drinks")
    public ResponseEntity<Object> createDrink(@Valid @RequestBody CDrink pDrinks) {
        try {
            pDrinks.setNgayTao(new Date());
            pDrinks.setNgayCapNhat(null);
            CDrink drink = pDrinkRepository.save(pDrinks);
            return new ResponseEntity<>(drink, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("++++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Drink: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/drinks/{id}")
    public ResponseEntity<Object> updateDrink(@PathVariable("id") long id, @Valid @RequestBody CDrink pDrinks) {
        try {
            CDrink drinkData = pDrinkRepository.findById(id);
            CDrink drink = drinkData;
            drink.setMaNuocUong(pDrinks.getMaNuocUong());
            drink.setTenNuocUong(pDrinks.getTenNuocUong());
            drink.setDonGia(pDrinks.getDonGia());
            drink.setGhiChu(pDrinks.getGhiChu());
            drink.setNgayCapNhat(new Date());
            try {
                return new ResponseEntity<>(pDrinkRepository.save(drink), HttpStatus.OK);
            } catch (Exception e) {
                System.out.println("++++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified Drink: " + e.getCause().getCause().getMessage());
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/drinks/{id}")
    public ResponseEntity<CDrink> deleteDrink(@PathVariable("id") long id) {
        try {
            pDrinkRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/drinks")
    public ResponseEntity<CDrink> deleteAllDrinks() {
        try {
            pDrinkRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }
}
