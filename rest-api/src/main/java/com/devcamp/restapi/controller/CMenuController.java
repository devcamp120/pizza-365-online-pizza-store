package com.devcamp.restapi.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapi.model.CMenu;
import com.devcamp.restapi.repository.IMenuRepository;

@CrossOrigin
@RestController
@RequestMapping
public class CMenuController {
    @Autowired
    private IMenuRepository menuRepository;

    @GetMapping("/menus")
    public ResponseEntity<List<CMenu>> getAllMenus() {
        try {
            List<CMenu> menus = new ArrayList<CMenu>();
            menuRepository.findAll().forEach(menus::add);
            return new ResponseEntity<>(menus, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/menus/{id}")
    public ResponseEntity<CMenu> getMenuById(@PathVariable("id") long id) {
        try {
            Optional<CMenu> menuOptional = menuRepository.findById(id);
            if (menuOptional.isPresent()) {
                return new ResponseEntity<>(menuOptional.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/menus")
    public ResponseEntity<Object> createMenu(@Valid @RequestBody CMenu pMenu) {
        try {
            CMenu menu = new CMenu(pMenu.getMenuName(), pMenu.getDuongKinhCM(), pMenu.getSuonNuong(),
                    pMenu.getSaladGr(), pMenu.getDrink(), pMenu.getPriceVND());
            CMenu _vocuher = menuRepository.save(menu);
            return new ResponseEntity<>(_vocuher, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("++++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Drink: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/menus/{id}")
    public ResponseEntity<Object> updateMenuById(@PathVariable("id") long id,
            @Valid @RequestBody CMenu pMenu) {
        try {
            Optional<CMenu> menuOptional = menuRepository.findById(id);
            if (menuOptional.isPresent()) {
                CMenu menu = menuOptional.get();
                menu.setMenuName(pMenu.getMenuName());
                menu.setDuongKinhCM(pMenu.getDuongKinhCM());
                menu.setSuonNuong(pMenu.getSuonNuong());
                menu.setSaladGr(pMenu.getSaladGr());
                menu.setDrink(pMenu.getDrink());
                menu.setPriceVND(pMenu.getPriceVND());
                CMenu _menu = menuRepository.save(menu);
                return new ResponseEntity<>(_menu, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println("++++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Drink: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/menus/{id}")
    public ResponseEntity<CMenu> deleteMenuById(@PathVariable("id") long id) {
        try {
            Optional<CMenu> menuOptional = menuRepository.findById(id);
            if (menuOptional.isPresent()) {
                menuRepository.deleteById(id);
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
