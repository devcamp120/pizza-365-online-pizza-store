package com.devcamp.restapi.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapi.model.CVoucher;
import com.devcamp.restapi.repository.IVoucherRepository;

@CrossOrigin
@RestController
@RequestMapping
public class CVoucherController {
    @Autowired
    private IVoucherRepository voucherRepository;

    @GetMapping("/vouchers")
    public ResponseEntity<List<CVoucher>> getAllVouchers() {
        try {
            List<CVoucher> vouchers = new ArrayList<CVoucher>();
            voucherRepository.findAll().forEach(vouchers::add);
            return new ResponseEntity<>(vouchers, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/vouchers/{id}")
    public ResponseEntity<CVoucher> getVoucherById(@PathVariable("id") long id) {
        try {
            Optional<CVoucher> voucherOptional = voucherRepository.findById(id);
            if (voucherOptional.isPresent()) {
                return new ResponseEntity<>(voucherOptional.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/vouchers")
    public ResponseEntity<Object> createVoucher(@Valid @RequestBody CVoucher pVoucher) {
        try {
            CVoucher voucher = new CVoucher(pVoucher.getMaVoucher(), pVoucher.getPhanTramGiamGia(),
                    pVoucher.getGhiChu(), new Date(), null);
            CVoucher _vocuher = voucherRepository.save(voucher);
            return new ResponseEntity<>(_vocuher, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("++++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Drink: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/vouchers/{id}")
    public ResponseEntity<Object> updateVoucherById(@PathVariable("id") long id,
            @Valid @RequestBody CVoucher pVoucher) {
        try {
            Optional<CVoucher> voucherOptional = voucherRepository.findById(id);
            if (voucherOptional.isPresent()) {
                CVoucher voucher = voucherOptional.get();
                voucher.setMaVoucher(pVoucher.getMaVoucher());
                voucher.setPhanTramGiamGia(pVoucher.getPhanTramGiamGia());
                voucher.setGhiChu(pVoucher.getGhiChu());
                voucher.setNgayCapNhat(new Date());
                CVoucher _voucher = voucherRepository.save(voucher);
                return new ResponseEntity<>(_voucher, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println("++++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Drink: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/vouchers/{id}")
    public ResponseEntity<CVoucher> deleteVoucherById(@PathVariable("id") long id) {
        try {
            Optional<CVoucher> voucherOptional = voucherRepository.findById(id);
            if (voucherOptional.isPresent()) {
                voucherRepository.deleteById(id);
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
