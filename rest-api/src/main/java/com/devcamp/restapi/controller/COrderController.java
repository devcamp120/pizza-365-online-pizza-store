package com.devcamp.restapi.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapi.model.COrder;
import com.devcamp.restapi.repository.IOrderRepository;

@CrossOrigin
@RestController
@RequestMapping
public class COrderController {
    @Autowired
    private IOrderRepository orderRepository;

    @GetMapping("/orders")
    public ResponseEntity<List<COrder>> getAllOrders() {
        try {
            List<COrder> listOrders = new ArrayList<COrder>();
            orderRepository.findAll().forEach(listOrders::add);
            return new ResponseEntity<>(listOrders, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/orders/{id}")
    public ResponseEntity<COrder> getOrderById(@PathVariable long id) {
        try {
            Optional<COrder> orderOptional = orderRepository.findById(id);
            if (orderOptional.isPresent()) {
                COrder _order = orderOptional.get();
                return new ResponseEntity<>(_order, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/orders")
    public ResponseEntity<Object> createOrder(@Valid @RequestBody COrder pOrder) {
        try {
            COrder order = new COrder(pOrder.getOrderCode(), pOrder.getPizzaSize(), pOrder.getPizzaType(),
                    pOrder.getVoucherCode(), pOrder.getPrice(), pOrder.getPaid(), new Date(), null);
            COrder _order = orderRepository.save(order);
            return new ResponseEntity<>(_order, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("++++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Order: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/orders/{id}")
    public ResponseEntity<Object> updateOrder(@PathVariable long id, @Valid @RequestBody COrder pOrder) {
        try {
            Optional<COrder> orderOptional = orderRepository.findById(id);
            if (orderOptional.isPresent()) {
                COrder order = orderOptional.get();
                order.setOrderCode(pOrder.getOrderCode());
                order.setPizzaSize(pOrder.getPizzaSize());
                order.setPizzaType(pOrder.getPizzaType());
                order.setVoucherCode(pOrder.getVoucherCode());
                order.setPrice(pOrder.getPrice());
                order.setPaid(pOrder.getPaid());
                order.setUpdateDate(new Date());
                COrder _order = orderRepository.save(order);
                return new ResponseEntity<>(_order, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println("++++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Order: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/orders/{id}")
    public ResponseEntity<COrder> deleteOrder(@PathVariable long id) {
        try {
            Optional<COrder> orderOptional = orderRepository.findById(id);
            if (orderOptional.isPresent()) {
                orderRepository.deleteById(id);
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
