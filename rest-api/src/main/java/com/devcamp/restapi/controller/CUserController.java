package com.devcamp.restapi.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapi.model.COrder;
import com.devcamp.restapi.model.CUser;
import com.devcamp.restapi.repository.IOrderRepository;
import com.devcamp.restapi.repository.IUserRepository;

@CrossOrigin
@RestController
@RequestMapping
public class CUserController {
    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IOrderRepository orderRepository;

    @GetMapping("/users")
    public ResponseEntity<List<CUser>> getAllUsers() {
        try {
            List<CUser> listUsers = new ArrayList<CUser>();
            userRepository.findAll().forEach(listUsers::add);
            return new ResponseEntity<>(listUsers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<CUser> getUserById(@PathVariable long id) {
        try {
            Optional<CUser> userOptional = userRepository.findById(id);
            if (userOptional.isPresent()) {
                CUser _user = userOptional.get();
                return new ResponseEntity<>(_user, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/users")
    public ResponseEntity<Object> createUser(@Valid @RequestBody CUser pUser) {
        try {
            CUser user = new CUser(pUser.getFullName(), pUser.getEmail(), pUser.getPhone(), pUser.getAddress(),
                    new Date(), null);
            CUser _user = userRepository.save(user);
            return new ResponseEntity<>(_user, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("++++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified User: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable long id, @Valid @RequestBody CUser pUser) {
        try {
            Optional<CUser> userOptional = userRepository.findById(id);
            if (userOptional.isPresent()) {
                CUser user = userOptional.get();
                user.setFullName(pUser.getFullName());
                user.setEmail(pUser.getEmail());
                user.setPhone(pUser.getPhone());
                user.setAddress(pUser.getAddress());
                user.setUpdateDate(new Date());
                CUser _user = userRepository.save(user);
                return new ResponseEntity<>(_user, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println("++++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified User: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<CUser> deleteUser(@PathVariable long id) {
        try {
            Optional<CUser> userOptional = userRepository.findById(id);
            if (userOptional.isPresent()) {
                CUser user = userOptional.get();
                Set<COrder> orders = user.getOrders();
                for (COrder order : orders) {
                    orderRepository.delete(order);
                }
                userRepository.deleteById(id);
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/users/{id}/orders")
    public ResponseEntity<Set<COrder>> getOrdersByUserId(@PathVariable long id) {
        try {
            Optional<CUser> userOptional = userRepository.findById(id);
            if (userOptional.isPresent()) {
                CUser user = userOptional.get();
                Set<COrder> orders = user.getOrders();
                return new ResponseEntity<>(orders, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
